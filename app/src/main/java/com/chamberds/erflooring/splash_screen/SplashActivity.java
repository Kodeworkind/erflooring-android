package com.chamberds.erflooring.splash_screen;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.Window;
import android.view.WindowManager;

import com.chamberds.erflooring.R;
import com.chamberds.erflooring.job_selection.JobSelectionActivity;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SplashActivity extends DaggerAppCompatActivity implements SplashScreenContract.View {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Inject
    SplashScreenContract.Presenter mPresenter;
    Disposable subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        subscription = Observable.timer(1000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> whatToDo());


        //TODO check if the app has authentication
        //TODO get properly list and then move to the next screen
    }

    private void whatToDo() {
        if (subscription != null && !subscription.isDisposed())
            subscription.dispose();

        startActivity(new Intent(SplashActivity.this, JobSelectionActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.takeView(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.dropView();
    }

    @Override
    public void showProgressLoader(boolean toShow) {

    }
}