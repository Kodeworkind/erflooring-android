package com.chamberds.erflooring.splash_screen;

import com.chamberds.erflooring.di.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Vaibhav Barad on 7/20/2018.
 */
@Module
public abstract class SplashScreenModule {

    @ActivityScoped
    @Binds
    abstract SplashScreenContract.Presenter presenter(SplashScreenPresenter presenter);

    @ActivityScoped
    @Binds
    abstract SplashScreenContract.View view(SplashActivity splashActivity);
}
