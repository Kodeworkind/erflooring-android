package com.chamberds.erflooring.splash_screen;

import com.chamberds.erflooring.base_class.BasePresenter;
import com.chamberds.erflooring.base_class.BaseView;

/**
 * Created by Vaibhav Barad on 7/20/2018.
 */
public interface SplashScreenContract {

    interface View extends BaseView<Presenter> {

    }

    interface Presenter extends BasePresenter<View> {


    }
}
