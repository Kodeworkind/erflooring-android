package com.chamberds.erflooring.utility;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.chamberds.erflooring.R;
import com.chamberds.erflooring.schedule_booking.ScheduleBookingActivity;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Vaibhav Barad on 7/25/2018.
 */
public class HighlightWeekendsDecorator implements DayViewDecorator {
    private final Calendar calendar = Calendar.getInstance(Locale.getDefault());
    private final Drawable highlightDrawable;
    private static final int color = Color.parseColor("#d8d8d8");

    public HighlightWeekendsDecorator(ScheduleBookingActivity scheduleBookingActivity) {
        highlightDrawable = ContextCompat.getDrawable(scheduleBookingActivity,R.drawable.calendar_disable);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        day.copyTo(calendar);
        int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
        return weekDay == Calendar.SUNDAY;
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setBackgroundDrawable(highlightDrawable);
    }
}
