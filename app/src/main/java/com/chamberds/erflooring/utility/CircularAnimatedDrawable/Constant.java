package com.chamberds.erflooring.utility.CircularAnimatedDrawable;

/**
 * Created by Vaibhav Barad on 5/8/2018.
 */
public class Constant {
    public static CircularButtonStatus mStatus = CircularButtonStatus.IDLE;

    public enum CircularButtonStatus {
        SUCCESSFUL, FAILED, IDLE
    }

    public static enum CustomStatus {
        SUCCESSFUL, FAILED, IDLE
    }
}
