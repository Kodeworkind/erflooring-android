package com.chamberds.erflooring.utility;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import java.text.DateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Vaibhav Barad on 5/24/2018.
 */
public class UtilityClass {

    /*Check if the necessary permissions were granted and return boolean */
    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static String getUTCTime() {
        DateFormat df = DateFormat.getTimeInstance();
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(new Date().getTime());
    }
}
