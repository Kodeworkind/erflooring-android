package com.chamberds.erflooring.utility;

import java.util.List;

/**
 * Created by Vaibhav Barad on 4/23/2018.
 */

public class ObjectUtil {
    public static boolean isEmpty(String string) {
        return string == null || string.length() == 0;
    }

    public static boolean isNull(Object obj) {
        return obj == null;
    }

    public static boolean isEmptyList(List list) {
        return list == null || list.isEmpty();
    }
}
