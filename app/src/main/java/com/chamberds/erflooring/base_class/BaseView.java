package com.chamberds.erflooring.base_class;

/**
 * Created by Vaibhav Barad on 5/24/2018.
 */
public interface BaseView<T> {
    void showProgressLoader(boolean toShow);
}