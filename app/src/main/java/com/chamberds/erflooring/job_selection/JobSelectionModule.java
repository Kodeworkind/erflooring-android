package com.chamberds.erflooring.job_selection;

import com.chamberds.erflooring.di.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Vaibhav Barad on 7/20/2018.
 */
@Module
public abstract class JobSelectionModule {
    @ActivityScoped
    @Binds
    abstract JobSelectionContract.Presenter presenter(JobSelectionPresenter presenter);

    @ActivityScoped
    @Binds
    abstract JobSelectionContract.View view(JobSelectionActivity activity);
}
