package com.chamberds.erflooring.job_selection;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.chamberds.erflooring.R;
import com.chamberds.erflooring.bottom_sheet.property_listing.PropertyBottomSheet;
import com.chamberds.erflooring.bottom_sheet.unit_listing.UnitSizeBottomSheet;
import com.chamberds.erflooring.schedule_booking.ScheduleBookingActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by Vaibhav Barad on 7/20/2018.
 */
public class JobSelectionActivity extends DaggerAppCompatActivity implements JobSelectionContract.View, UnitSizeBottomSheet.ItemClickListener {

    @Inject
    JobSelectionContract.Presenter mPresenter;
    @BindView(R.id.property_value)
    TextView propertyValue;
    @BindView(R.id.unit_size_value)
    TextView unitSizeValue;
    @BindView(R.id.unit_no_value)
    TextView unitNoValue;
    @BindView(R.id.installation_switch)
    SwitchCompat installationSwitch;
    @BindView(R.id.repair_switch)
    SwitchCompat repairSwitch;
    @BindView(R.id.installation_list)
    RecyclerView installationList;
    @BindView(R.id.repair_list)
    RecyclerView repairList;
    @BindView(R.id.rg_unit_occupancy)
    RadioGroup rgUnit;
    @BindView(R.id.rg_pte_occupancy)
    RadioGroup rgPTE;
    @BindView(R.id.rg_time_occupancy)
    RadioGroup rgTime;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.btn_continue)
    Button btnContinue;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_selection);
        ButterKnife.bind(this);
        setupToolbar();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void showProgressLoader(boolean toShow) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.takeView(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.dropView();
    }

    @OnClick({R.id.property_value, R.id.unit_size_value, R.id.btn_continue})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.property_value:
                showBottomSheet(false);
                break;
            case R.id.unit_size_value:
                showBottomSheet(true);
                break;
            case R.id.btn_continue:
                startActivity(new Intent(JobSelectionActivity.this, ScheduleBookingActivity.class));
                break;
        }
    }

    private void showBottomSheet(boolean isUnitSelection) {
        BottomSheetDialogFragment fragment;
        if (isUnitSelection) {
            fragment = new UnitSizeBottomSheet();
        } else {
            fragment = new PropertyBottomSheet();
        }
        fragment.show(getSupportFragmentManager(), fragment.getTag());
    }

    @Override
    public void OnItemSelected(boolean isUnitSelection, int index, String unitName) {
        if(isUnitSelection){
            unitSizeValue.setText(unitName);
        }

    }
}
