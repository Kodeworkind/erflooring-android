package com.chamberds.erflooring.job_selection;

import com.chamberds.erflooring.data.DataSource;
import com.chamberds.erflooring.data.Repository;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Vaibhav Barad on 7/20/2018.
 */
class JobSelectionPresenter implements JobSelectionContract.Presenter {
    private JobSelectionContract.View mView;
    private DataSource mDataSource;

    private CompositeDisposable compositeDisposable;
    private Disposable userDisposable;

    @Inject
    public JobSelectionPresenter(JobSelectionContract.View view, Repository mDataSource, CompositeDisposable compositeDisposable) {
        this.mDataSource = mDataSource;
        this.mView = view;
        this.compositeDisposable = compositeDisposable;
    }


    @Override
    public void takeView(JobSelectionContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            //compositeDisposable.remove(userDisposable);
            compositeDisposable.clear();
        }
    }
}
