package com.chamberds.erflooring.data.module;

import android.app.Application;

import com.chamberds.erflooring.data.local.DatabaseHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
@Module
public class DatabaseModule {

    @Singleton
    @Provides
    DatabaseHelper provideDatabaseHelper(Application application) {
        return new DatabaseHelper(application);
    }
}
