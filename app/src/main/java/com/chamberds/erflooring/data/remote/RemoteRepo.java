package com.chamberds.erflooring.data.remote;

import android.support.annotation.NonNull;

import com.chamberds.erflooring.data.DataSource;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Retrofit;

import static com.chamberds.erflooring.data.remote.ServiceError.ERROR_UNDEFINED;
import static com.chamberds.erflooring.data.remote.ServiceError.NETWORK_ERROR;
import static com.chamberds.erflooring.utility.ObjectUtil.isNull;

/**
 * Implementation of the DataSource as Retrofit(Network Calls).
 * Reference : https://github.com/ahmedeltaher/MVP-RX-Android-Sample
 */
@Singleton
public class RemoteRepo implements DataSource {

    public static final String TAG = RemoteRepo.class.getSimpleName();

    Retrofit mRetrofit;
    private RetrofitAPI retrofitAPI;

    @Inject
    public RemoteRepo(Retrofit retrofit, RetrofitAPI api) {
        this.mRetrofit = retrofit;
        this.retrofitAPI = api;
    }

    /*@Override
    public Single<LoginModel> performLogin(String requestBody) {
        return Single.create(emitter -> {
            ServiceResponse response = processCall(retrofitAPI.loginUser(JSONToRetrofitJSON(requestBody)), false);
            if (response.getCode() == SUCCESS_CODE) {
                emitter.onSuccess((LoginModel) response.getData());
            } else {
                Throwable throwable = new NetworkErrorException();
                emitter.onError(throwable);
            }
        });
    }*/

    @NonNull
    private ServiceResponse processCall(Call call, boolean isVoid) {
        try {
            retrofit2.Response response = call.execute();
            if (isNull(response)) {
                return new ServiceResponse(new ServiceError(NETWORK_ERROR, ERROR_UNDEFINED));
            }
            int responseCode = response.code();
            if (response.isSuccessful()) {
                return new ServiceResponse(responseCode, isVoid ? null : response.body());
            } else {
                ServiceError ServiceError;
                ServiceError = new ServiceError(response.message(), responseCode);
                return new ServiceResponse(ServiceError);
            }
        } catch (IOException e) {
            return new ServiceResponse(new ServiceError(NETWORK_ERROR, ERROR_UNDEFINED));
        }
    }
}