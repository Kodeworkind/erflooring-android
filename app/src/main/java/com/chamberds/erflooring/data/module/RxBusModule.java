package com.chamberds.erflooring.data.module;

import com.chamberds.erflooring.data.RxBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Vaibhav Barad on 02-04-2018.
 */

@Module
public class RxBusModule {

    @Singleton
    @Provides
    RxBus provideRxBus() {
        return new RxBus();
    }
}
