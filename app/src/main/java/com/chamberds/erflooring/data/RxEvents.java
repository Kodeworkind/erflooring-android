package com.chamberds.erflooring.data;

/**
 * Created by Vaibhav Barad on 5/24/2018.
 */
public class RxEvents {

    public static class PermissionEvent {
    }

    public static class LoginStatus {
        boolean isSuccess;

        public LoginStatus(boolean isSuccess) {
            this.isSuccess = isSuccess;
        }

        public boolean isSuccess() {
            return isSuccess;
        }

        public void setSuccess(boolean success) {
            isSuccess = success;
        }
    }
}
