package com.chamberds.erflooring.data.module;

import com.chamberds.erflooring.data.DataSource;
import com.chamberds.erflooring.data.Repository;
import com.chamberds.erflooring.data.local.LocalRepo;
import com.chamberds.erflooring.data.remote.RemoteRepo;
import com.chamberds.erflooring.data.scopes.Local;
import com.chamberds.erflooring.data.scopes.Remote;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

/**
 * This is used by Dagger to inject the required arguments into the {@link Repository}.
 */
@Module
abstract public class RepositoryModule {

    @Singleton
    @Binds
    @Local
    abstract DataSource provideLocalDataSource(LocalRepo dataSource);

    @Singleton
    @Binds
    @Remote
    abstract DataSource provideRemoteDataSource(RemoteRepo dataSource);

}