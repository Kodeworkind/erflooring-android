package com.chamberds.erflooring.data;


import com.chamberds.erflooring.data.scopes.Local;
import com.chamberds.erflooring.data.scopes.Remote;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class Repository implements DataSource {
    private final DataSource mRemoteDataSource;
    private final DataSource mLocalDataSource;

    @Inject
    public Repository(@Remote DataSource mRemoteDataSource, @Local DataSource mLocalDataSource) {
        this.mRemoteDataSource = mRemoteDataSource;
        this.mLocalDataSource = mLocalDataSource;
    }

}