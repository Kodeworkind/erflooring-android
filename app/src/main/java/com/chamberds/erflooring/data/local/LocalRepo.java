package com.chamberds.erflooring.data.local;

import com.chamberds.erflooring.data.DataSource;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Concrete implementation of DataSource as a Realm/Room/Sqlite.
 */
@Singleton
public class LocalRepo implements DataSource {
    DatabaseHelper mDatabaseHelper;

    @Inject
    public LocalRepo(DatabaseHelper mDatabaseHelper) {
        this.mDatabaseHelper = mDatabaseHelper;
    }
}