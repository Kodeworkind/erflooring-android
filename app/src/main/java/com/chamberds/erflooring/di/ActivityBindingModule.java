package com.chamberds.erflooring.di;

import com.chamberds.erflooring.job_selection.JobSelectionActivity;
import com.chamberds.erflooring.job_selection.JobSelectionModule;
import com.chamberds.erflooring.schedule_booking.ScheduleBookingActivity;
import com.chamberds.erflooring.schedule_booking.ScheduleBookingModule;
import com.chamberds.erflooring.splash_screen.SplashActivity;
import com.chamberds.erflooring.splash_screen.SplashScreenModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * We want Dagger.Android to create a Subcomponent which has a parent Component of whichever module ActivityBindingModule is on,
 * in our case that will be AppComponent. The beautiful part about this setup is that you never need to tell AppComponent that it is going to have all these subcomponents
 * nor do you need to tell these subcomponents that AppComponent exists.
 * <p>
 * We are also telling Dagger.Android that this generated SubComponent needs to include the specified modules and be aware of a
 * scope annotation @ActivityScoped
 * When Dagger.Android annotation processor runs it will create 1 subcomponent for us.
 */
@Module
public abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = JobSelectionModule.class)
    abstract JobSelectionActivity jobSelectionActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ScheduleBookingModule.class)
    abstract ScheduleBookingActivity scheduleBookingActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = SplashScreenModule.class)
    abstract SplashActivity splashActivity();

}