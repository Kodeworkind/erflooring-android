package com.chamberds.erflooring.di;

import android.app.Application;

import com.chamberds.erflooring.ERFlooringApplication;
import com.chamberds.erflooring.data.module.DatabaseModule;
import com.chamberds.erflooring.data.module.NetworkModule;
import com.chamberds.erflooring.data.module.RepositoryModule;
import com.chamberds.erflooring.data.module.RxBusModule;
import com.chamberds.erflooring.data.module.SharedPrefModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by Vaibhav Barad on 5/23/2018.
 */

/**
 * This is a Dagger component. Refer to {@link ERFlooringApplication} for the list of Dagger components
 * used in this application.
 * <p>
 * Even though Dagger allows annotating a {@link Component} as a singleton, the code
 * itself must ensure only one instance of the class is created. This is done in {@link
 * ERFlooringApplication}.
 * <p>
 * {@link AndroidSupportInjectionModule} is the module from Dagger.Android that helps with the generation
 * and location of subcomponents.
 */
@Singleton
@Component(modules = {
        RepositoryModule.class,
        ApplicationModule.class,
        NetworkModule.class,
        RxBusModule.class,
        DatabaseModule.class,
        SharedPrefModule.class,
        ActivityBindingModule.class,
        AndroidSupportInjectionModule.class})
public interface AppComponent extends AndroidInjector<ERFlooringApplication> {

    void inject(ERFlooringApplication app);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}