package com.chamberds.erflooring;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.chamberds.erflooring.job_selection.JobSelectionActivity;
import com.chamberds.erflooring.schedule_booking.ScheduleBookingActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Vaibhav Barad on 7/24/2018.
 */
public class ScheduleComplete extends AppCompatActivity {

    @BindView(R.id.btn_continue2)
    Button finish_schedule;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thank_you);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_continue2)
    public void onViewClicked() {
        startActivity(new Intent(ScheduleComplete.this, JobSelectionActivity.class));
        finish();
    }
}
