package com.chamberds.erflooring.schedule_booking;

import com.chamberds.erflooring.base_class.BasePresenter;
import com.chamberds.erflooring.base_class.BaseView;

/**
 * Created by Vaibhav Barad on 7/20/2018.
 */
public interface ScheduleBookingContract {
    interface View extends BaseView<Presenter> {

    }

    interface Presenter extends BasePresenter<View> {


    }
}
