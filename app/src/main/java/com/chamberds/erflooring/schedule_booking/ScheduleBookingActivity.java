package com.chamberds.erflooring.schedule_booking;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.chamberds.erflooring.R;
import com.chamberds.erflooring.ScheduleComplete;
import com.chamberds.erflooring.utility.HighlightWeekendsDecorator;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.format.ArrayWeekDayFormatter;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by Vaibhav Barad on 7/20/2018.
 */
public class ScheduleBookingActivity extends DaggerAppCompatActivity implements ScheduleBookingContract.View, OnDateSelectedListener {

    @Inject
    ScheduleBookingContract.Presenter mPresenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.calendarView)
    MaterialCalendarView calendarView;

    @BindView(R.id.btn_continue)
    Button btnContinue;
    @BindView(R.id.special_instructions)
    EditText specialInstructions;
    CalendarDay today;

    @BindView(R.id.pet_damage)
    CheckBox pet_damage;
    @BindView(R.id.wear_and_tear)
    CheckBox wear_and_tear;
    @BindView(R.id.tenant_damage)
    CheckBox tenant_damage;

    private boolean sundayCriteriaEnabled;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_booking);
        ButterKnife.bind(this);

        setupToolbar();
        initCalendar();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCalendar() {
        today = CalendarDay.today();
        calendarView.state().edit().setMinimumDate(today)
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setCalendarDisplayMode(CalendarMode.MONTHS)
                .commit();

        calendarView.setTileHeightDp(36);
        calendarView.setWeekDayFormatter(new ArrayWeekDayFormatter(getResources().getTextArray(R.array.custom_weekdays)));
        calendarView.addDecorator(new HighlightWeekendsDecorator(ScheduleBookingActivity.this));
        calendarView.setOnDateChangedListener(this);

    }

    @Override
    public void showProgressLoader(boolean toShow) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.takeView(this);
        calendarView.setOnDateChangedListener(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        calendarView.setOnDateChangedListener(null);
        mPresenter.dropView();
    }

    @OnClick(R.id.btn_continue)
    public void onViewClicked() {
        startActivity(new Intent(ScheduleBookingActivity.this, ScheduleComplete.class));
        finish();
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        Toast.makeText(this, "date selected=> " + date.toString() + " size => " + calendarView.getSelectedDates().size(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    static class IncludedLayout {
        @BindView(R.id.checkboxItem)
        CheckBox checkBox;
    }
}
