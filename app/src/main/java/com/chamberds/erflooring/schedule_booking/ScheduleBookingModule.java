package com.chamberds.erflooring.schedule_booking;

import com.chamberds.erflooring.di.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Vaibhav Barad on 7/20/2018.
 */
@Module
public abstract class ScheduleBookingModule {
    @ActivityScoped
    @Binds
    abstract ScheduleBookingContract.Presenter presenter(ScheduleBookingPresenter presenter);

    @ActivityScoped
    @Binds
    abstract ScheduleBookingContract.View view(ScheduleBookingActivity scheduleBookingActivity);
}
