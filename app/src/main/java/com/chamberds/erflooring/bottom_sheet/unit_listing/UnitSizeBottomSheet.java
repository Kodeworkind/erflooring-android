package com.chamberds.erflooring.bottom_sheet.unit_listing;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chamberds.erflooring.R;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Vaibhav Barad on 7/21/2018.
 */
public class UnitSizeBottomSheet extends BottomSheetDialogFragment implements UnitSizeAdapter.ItemClickListener {

    @BindView(R.id.bottom_sheet_list)
    RecyclerView bottomSheetList;
    @BindView(R.id.sheet_name)
    TextView sheetName;
    ArrayList<String> unitSizeList;
    Context mContext;
    ItemClickListener listener;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet, container, false);
        ButterKnife.bind(this, view);
        sheetName.setText(R.string.select_size);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRecyclerView();
    }

    private void setupRecyclerView() {

        String[] myResArray = getResources().getStringArray(R.array.unit_size);
        unitSizeList = new ArrayList<>(Arrays.asList(myResArray));
        bottomSheetList.setLayoutManager(new LinearLayoutManager(mContext));
        bottomSheetList.setAdapter(new UnitSizeAdapter(UnitSizeBottomSheet.this, unitSizeList));

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mContext = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if (context instanceof ItemClickListener)
            listener = (ItemClickListener) context;
    }

    @Override
    public void OnItemSelected(int position) {
        dismiss();
        listener.OnItemSelected(true,position + 1, unitSizeList.get(position));
    }

    public interface ItemClickListener {
        void OnItemSelected(boolean isUnitSelection, int searchIndex, String unitName);
    }
}
