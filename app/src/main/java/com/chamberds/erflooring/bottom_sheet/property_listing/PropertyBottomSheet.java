package com.chamberds.erflooring.bottom_sheet.property_listing;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chamberds.erflooring.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Vaibhav Barad on 7/21/2018.
 */
public class PropertyBottomSheet extends BottomSheetDialogFragment {

    @BindView(R.id.bottom_sheet_list)
    RecyclerView bottomSheetList;
    @BindView(R.id.sheet_name)
    TextView sheetName;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.bottom_sheet, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
