package com.chamberds.erflooring.bottom_sheet.unit_listing;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Vaibhav Barad on 7/25/2018.
 */
class UnitSizeAdapter extends RecyclerView.Adapter<UnitSizeAdapter.ViewHolder> {
    private final UnitSizeBottomSheet unitSizeBottomSheet;
    private final ArrayList<String> unitSizeList;
    ItemClickListener listener;

    interface ItemClickListener{
        void OnItemSelected(int position);
    }

    public UnitSizeAdapter(UnitSizeBottomSheet unitSizeBottomSheet, ArrayList<String> unitSizeList) {
        this.unitSizeBottomSheet = unitSizeBottomSheet;
        this.unitSizeList = unitSizeList;
        listener = unitSizeBottomSheet;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.data.setText(unitSizeList.get(position));
    }

    @Override
    public int getItemCount() {
        return unitSizeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView data;
        public ViewHolder(View itemView) {
            super(itemView);
            data = itemView.findViewById(android.R.id.text1);
            itemView.setOnClickListener(v -> listener.OnItemSelected(getAdapterPosition()));
        }
    }
}
