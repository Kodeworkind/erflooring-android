package com.chamberds.erflooring.bottom_sheet.unit_listing;

import com.chamberds.erflooring.base_class.BasePresenter;
import com.chamberds.erflooring.base_class.BaseView;

/**
 * Created by Vaibhav Barad on 7/21/2018.
 */
public interface UnitSizeContract {
    interface View extends BaseView<Presenter>{

    }
    interface Presenter extends BasePresenter<View>{

    }
}
