package com.chamberds.erflooring.bottom_sheet.unit_listing;

import com.chamberds.erflooring.data.DataSource;
import com.chamberds.erflooring.data.Repository;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Vaibhav Barad on 7/21/2018.
 */
public class UnitSizePresenter implements UnitSizeContract.Presenter {
    private UnitSizeContract.View mView;

    private DataSource mDataSource;
    private CompositeDisposable compositeDisposable;
    private Disposable userDisposable;

    @Inject
    public UnitSizePresenter(UnitSizeContract.View view, Repository mDataSource, CompositeDisposable compositeDisposable) {
        this.mDataSource = mDataSource;
        this.mView = view;
        this.compositeDisposable = compositeDisposable;
    }

    @Override
    public void takeView(UnitSizeContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.remove(userDisposable);
            compositeDisposable.clear();
        }
    }
}
