package com.chamberds.erflooring.bottom_sheet.unit_listing;

import com.chamberds.erflooring.di.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Vaibhav Barad on 7/21/2018.
 */
@Module
public abstract class UnitSizeModule {
    @ActivityScoped
    @Binds
    abstract UnitSizeContract.Presenter presenter(UnitSizePresenter presenter);
}
